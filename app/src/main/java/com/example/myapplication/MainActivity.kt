package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.Log.d
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity() : AppCompatActivity(), Parcelable {
    constructor(parcel: Parcel) : this() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        val generaterandomnumberbutton = findViewById<Button>(R.id.generaterandomnumberbutton)
        val randomnumbertextview = findViewById<TextView>(R.id.randomnumbertextview)
        val edittext = findViewById<EditText>(R.id.edittext)
        generaterandomnumberbutton.setOnClickListener {
            val number:Int = randomNumber()
            d( "ButtonClicked", "$number" )
            randomnumbertextview.text = number.toString()
        }
    }
    private fun randomNumber():Int {
        val number:Int = (-101..101).random()
        if (number%5 == 0 && number>0) {
            edittext.setText("yes")}

            if (number%5 == 0 && number<0){
                edittext.setText("no")}

                if (number%5!=0)
                    edittext.setText("")
        return number
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MainActivity> {
        override fun createFromParcel(parcel: Parcel): MainActivity {
            return MainActivity(parcel)
        }

        override fun newArray(size: Int): Array<MainActivity?> {
            return arrayOfNulls(size)
        }
    }
}